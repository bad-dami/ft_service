/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.ftservice.services;

import com.kyriba.ftservice.externelservices.MdServiceProxy;
import com.kyriba.ftservice.model.ExchangeRate;
import com.kyriba.ftservice.model.FinancialEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.concurrent.atomic.LongAdder;

import static org.springframework.web.reactive.function.server.ServerResponse.ok;


/**
 * @author badr.dami
 * @since 19.2
 */
@Log4j2
@Component
@RequiredArgsConstructor
public class FTService
{
  final MdServiceProxy mdService;


  public Mono<ServerResponse> convert(final ServerRequest request)
  {
    final String srcCcy = request.pathVariable("srcCcy");
    final String dstCcy = request.pathVariable("dstCcy");
    final BigDecimal srcAmount = new BigDecimal(request.pathVariable("amount"));

    final LongAdder evictionStat = new LongAdder();
    final LongAdder evictionCount = new LongAdder();

    final ExchangeRate lastKnownRate = ExchangeRate.of(srcCcy, dstCcy, BigDecimal.valueOf(1));


    //final Flux<ExchangeRate> rates = mdService.getRatesServerSentEvents(srcCcy, dstCcy);
    final Flux<ExchangeRate> rates = mdService.getRatesUsingRSocket(srcCcy, dstCcy);

    final Flux<FinancialEvent> events = rates
        .retryBackoff(10, Duration.ofMillis(10L))
        .onErrorResume(throwable -> Flux.just(lastKnownRate))
        .onBackpressureBuffer(Duration.ofSeconds(1L), 5, rate -> {
          evictionStat.increment();
          evictionCount.increment();
        })
        .delayElements(Duration.ofSeconds(2L))
        .doOnEach(signal -> {
          log.info("Eviction summary: { stat: " + evictionStat + ", count: " + evictionCount + "}");
          evictionStat.reset();
        })
        .map(rate -> FinancialEvent.of(srcAmount.multiply(rate.getRate())));

    return ok()
        .contentType(MediaType.TEXT_EVENT_STREAM)
        .body(events, FinancialEvent.class);
  }
}
