/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.ftservice.externelservices;

import com.kyriba.ftservice.model.ExchangeRate;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.MediaType;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;


/**
 * @author badr.dami
 * @since 19.2
 */
@Log4j2
@Service
@RequiredArgsConstructor
public class MdServiceProxy
{
  private final WebClient webClient;
  private final RSocketRequester rSocketRequester;


  public Flux<ExchangeRate> getRatesServerSentEvents(final String srcCcyId, final String dstCcyId)
  {
    return webClient.get()
        .uri(uriBuilder -> uriBuilder.path("localhost:8081/rate/stream")
            .queryParam("srcCcyId", srcCcyId)
            .queryParam("dstCcyId", dstCcyId).build()
        )
        .accept(MediaType.TEXT_EVENT_STREAM)
        .retrieve()
        .bodyToFlux(ExchangeRate.class);
  }


  public Flux<ExchangeRate> getRatesUsingRSocket(final String srcCcyId, final String dstCcyId)
  {
    final RateRequest request = new RateRequest(srcCcyId, dstCcyId);
    return rSocketRequester
        .route("rateStream")
        .data(request)
        .retrieveFlux(ExchangeRate.class)
        .doOnEach(s -> log.info("RSocket event: " + s.get()));

  }

}