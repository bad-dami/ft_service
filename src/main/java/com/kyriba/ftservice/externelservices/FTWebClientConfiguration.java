/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.ftservice.externelservices;

import io.rsocket.transport.netty.client.TcpClientTransport;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.web.reactive.function.client.WebClient;


/**
 * @author badr.dami
 * @since 19.2
 */
@Configuration
class FTWebClientConfiguration
{

  @Bean
  public WebClient webClient(final WebClient.Builder builder)
  {
    return builder
        .build();
  }


  @Bean
  RSocketRequester rSocketRequester(final RSocketRequester.Builder builder)
  {
    return builder
        .connect(TcpClientTransport.create(7070))
        .block();
  }

}
