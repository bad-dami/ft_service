/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.ftservice.ftcontrollers;

import com.kyriba.ftservice.services.FTService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;

import static org.springframework.web.reactive.function.server.RouterFunctions.route;


/**
 * @author badr.dami
 * @since 19.2
 */
@Configuration
public class FTController
{
  @Bean
  public RouterFunction routes(final FTService ftService)
  {
    return route()
        .GET("/convert/{srcCcy}/{dstCcy}/{amount}", ftService::convert)
        .build();

  }
}

