/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.ftservice.externelservices;

import lombok.Value;


/**
 * @author badr.dami
 * @since 19.2
 */
@Value
class RateRequest
{
  final String srcCcyName;
  final String dstCcyName;
}
