/*
 * Copyright 2000 - 2019  Kyriba Corp. All Rights Reserved
 * The content of this file is copyrighted by Kyriba Corporation and can not be
 * reproduced, distributed, altered or used in any form, in whole or in part
 */
package com.kyriba.ftservice.model;

import lombok.Builder;
import lombok.Value;

import java.math.BigDecimal;
import java.time.Instant;


/**
 * @author badr.dami
 * @since 19.2
 */
@Value
@Builder
public class FinancialEvent
{
  final String eventInfo;
  final BigDecimal amount;
  final Instant time;


  public static FinancialEvent of(final BigDecimal amount)
  {
    return builder()
        .eventInfo("Exchange Simulation")
        .amount(amount)
        .time(Instant.now())
        .build();
  }
}
