package com.kyriba.ftservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class FtServiceApplication
{

  public static void main(String[] args)
  {
    SpringApplication.run(FtServiceApplication.class, args);
  }

}
